#!/bin/bash

. "./portage/cnf/make.globals"

# needs around 709 MB free
PORTAGE_TMPDIR=`readlink -f "./portage_tmp"`
PORTDIR=`readlink -f "./portdir"`

mkdir -p "$PORTAGE_TMPDIR"
mkdir -p "$PORTDIR"

export DISTDIR EPREFIX FEATURES \
       FETCHCOMMAND GENTOO_MIRRORS \
       PORTAGE_BIN_PATH PORTAGE_CONFIGROOT PORTAGE_GPG_DIR \
       PORTAGE_NICENESS PORTAGE_RSYNC_EXTRA_OPTS \
       PORTAGE_RSYNC_OPTS PORTAGE_TMPDIR PORTDIR \
       SYNC USERLAND http_proxy

"./portage/bin/emerge-webrsync"

rm -r "$PORTAGE_TMPDIR"
